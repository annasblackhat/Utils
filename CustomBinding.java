package id.pupm.tokotaniindonesia.util;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import id.pupm.tokotaniindonesia.R;

/**
 * Created by ANNASBlackHat on 06/10/2017.
 */

public class CustomBinding {

    @BindingAdapter(("imgUrl"))
    public static void setImageUrl(ImageView img, String url) {
        Glide.with(img.getContext())
                .load(url)
                .placeholder(R.drawable.default_image)
                .into(img);
    }

    @BindingAdapter({"circleImgUrl"})
    public static void setImageUrlTextRound(CircleImageView img, String url) {
        Glide.with(img.getContext())
                .load(url)
                .placeholder(R.drawable.default_image)
                .into(img);
    }

//    @BindingAdapter({"imgUrlText", "name"})
//    public static void setImageUrlText(ImageView img, String url, String name) {
//        if (url != null) {
//            Drawable placeholder = img.getResources().getDrawable(R.drawable.default_image);
//            if (name != null) {
//                name = name.trim();
//                String teks = "A";
//                if (name != null && name.length() > 1) teks = name.substring(0, 2);
//                if (name != null && name.contains(" "))
//                    teks = name.substring(0, 1) + name.substring(name.lastIndexOf(" ") + 1, name.lastIndexOf(" ") + 2);
//                ColorGenerator generator = ColorGenerator.MATERIAL;
//                placeholder = TextDrawable.builder()
//                        .buildRect(teks.toUpperCase(), generator.getColor(teks));
//            }
//            url = "http://uniq.tk/assets/products/1/" + url;
//            Glide.with(img.getContext())
//                    .load(url)
//                    .placeholder(placeholder)
//                    .into(img);
//        }
//    }

    @BindingAdapter("imgDrawable")
    public static void setImgDrawable(ImageView img, int drawable) {
        Glide.with(img.getContext())
                .load(drawable)
                .placeholder(R.drawable.default_image)
                .into(img);
    }

    @BindingAdapter("timeFromDate")
    public static void setTimeFromDate(TextView txt, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        txt.setText(sdf.format(date));
    }

    @BindingAdapter(value = {"textCurrency", "symbol"}, requireAll = false)
    public static void setTextCurrency(TextView txt, int value, String symbol) {
        String currency = NumberFormat.getNumberInstance().format(value);
        if(symbol != null) currency = symbol+currency;
        txt.setText(currency);
    }

    @BindingAdapter({"dateFromat","format"})
    public static void setTextDate(TextView txt, Date date,  String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        txt.setText(sdf.format(date));
    }


}
